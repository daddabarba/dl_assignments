"""
This module implements training and evaluation of a multi-layer perceptron in PyTorch.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import numpy as np
import os
from mlp_pytorch import MLP
import cifar10_utils

import torch
from torch import nn
from torch.optim import Adam

import matplotlib.pyplot as plt

# Default constants
DNN_HIDDEN_UNITS_DEFAULT = '1000, 500, 200, 100, 50'
LEARNING_RATE_DEFAULT = 2e-4
MAX_STEPS_DEFAULT = 1500
BATCH_SIZE_DEFAULT = 400
EVAL_FREQ_DEFAULT = 100
NEG_SLOPE_DEFAULT = 0.02

# Directory in which cifar data is saved
DATA_DIR_DEFAULT = './cifar10/cifar-10-batches-py'

FLAGS = None

def accuracy(predictions, targets):
	"""
	Computes the prediction accuracy, i.e. the average of correct predictions
	of the network.

	Args:
	predictions: 2D float array of size [batch_size, n_classes]
	labels: 2D int array of size [batch_size, n_classes]
			with one-hot encoding. Ground truth labels for
			each sample in the batch
	Returns:
	accuracy: scalar float, the accuracy of predictions,
			  i.e. the average correct predictions over the whole batch

	TODO:
	Implement accuracy computation.
	"""

	########################
	# PUT YOUR CODE HERE	#
	#######################

	# Convert inputs to numpy arrays

	if isinstance(predictions, torch.Tensor):
		predictions = predictions.detach().numpy()

	if isinstance(targets, torch.Tensor):
		targets = targets.detach().numpy()

	batch_size = predictions.shape[0]

	# Get one hot encoded of predicted class (best guess)
	predictions_onehot = np.zeros(predictions.shape)
	predictions_onehot[np.arange(0, batch_size), np.argmax(predictions, 1)] = 1.0

	# Get one hot encoding of correct predictions (predicted class is the same as target)
	predictions_correct = predictions_onehot*targets # Conjuction of targets and predictions (one hot encoded)

	# Average to get accuracy
	accuracy = predictions_correct.sum()/batch_size

	########################
	# END OF YOUR CODE	#
	#######################

	return accuracy

def train():
	"""
	Performs training and evaluation of MLP model. 

	TODO:
	Implement training and evaluation of MLP model. Evaluate your model on the whole test set each eval_freq iterations.
	"""

	### DO NOT CHANGE SEEDS!
	# Set the random seeds for reproducibility
	np.random.seed(42)

	## Prepare all functions
	# Get number of units in each hidden layer specified in the string such as 100,100
	if FLAGS.dnn_hidden_units:
		dnn_hidden_units = FLAGS.dnn_hidden_units.split(",")
		dnn_hidden_units = [int(dnn_hidden_unit_) for dnn_hidden_unit_ in dnn_hidden_units]
	else:
		dnn_hidden_units = []

	# Get negative slope parameter for LeakyReLU
	neg_slope = FLAGS.neg_slope

	########################
	# PUT YOUR CODE HERE	#
	#######################

	# Get parameters
	batch_size = FLAGS.batch_size
	eval_freq = FLAGS.eval_freq
	max_steps = FLAGS.max_steps
	eta = FLAGS.learning_rate

	count_eval = 0
	accuracies = []
	errors = []

	errors_train = []
	accuracies_train = []

	errors_train_partial = np.zeros(eval_freq)
	accuracies_train_partial = np.zeros(eval_freq)

	# Load the data
	dataset = cifar10_utils.get_cifar10(FLAGS.data_dir)

	training_set = dataset['train']
	test_set = dataset['test']

	train_size = training_set.num_examples

	num_batches = int(train_size/batch_size)
	max_epochs = int(max_steps/num_batches)

	# Get data-set parameters
	n_inputs = int(training_set.images.size/training_set.num_examples)
	n_outputs = training_set.labels.shape[1]

	# Pre-process test-set
	test_input = torch.Tensor(test_set.images.reshape(test_set.num_examples, -1))
	test_targets = test_set.labels
	test_targets_tensor = torch.LongTensor(np.argmax(test_targets, 1))

	# Init MLP
	mlp = MLP(n_inputs, dnn_hidden_units, n_outputs, neg_slope)

	# Init error function
	Loss = nn.CrossEntropyLoss()

	# Init optimizer
	optimizer = Adam(mlp.parameters(), eta)

	# Run max_steps epochs
	for epoch in range(max_epochs):

		for batch in range(num_batches):

			# Sample batch
			input_batch, targets = training_set.next_batch(batch_size)

			# Convert target to torch tensor
			t = torch.LongTensor(np.argmax(targets,1))

			# Convert batch to input matrix
			x = torch.Tensor(input_batch.reshape(batch_size, -1))

			# Compute network output
			y = mlp.forward(x)

			# Compute loss
			err = Loss(y,t)

			# Store error
			errors_train_partial[count_eval] = err

			# Compute accuracy
			accuracies_train_partial[count_eval] = accuracy(y, targets)

			# Compute gradient backward for all parameters
			optimizer.zero_grad()
			err.backward()

			# Evaluate model
			if count_eval == 0:

				# Compute predictions for test set
				y_test = mlp.forward(test_input)

				# Compute loss for test set
				errors.append(Loss(y_test, test_targets_tensor))

				# Compute accuracy for test set
				accuracies.append(accuracy(y_test, test_targets))

				# Save accuracies and loss for train set so far
				errors_train.append(np.mean(errors_train_partial))
				accuracies_train.append(np.mean(accuracies_train_partial))

				errors_train_partial *= 0
				accuracies_train_partial *= 0

				# Reset frequency count
				count_eval = 0

			# Update parameters using GD update rule
			optimizer.step()

			# Update count for accuracy evaluation frequency
			count_eval = (count_eval + 1)%eval_freq

			# Show progress
			percentage = (num_batches*epoch + batch)/max_steps
			idx = int(percentage*100)
			if not FLAGS.NGUI:
				print("\r(step: %d/%d, epoch: %d/%d)\t loss: %.3f - accuracy: %.3f\t[%s]"%(num_batches*epoch + batch, max_steps, epoch, max_epochs, errors[-1], accuracies[-1], '='*(idx-1) + '>' + ' '*(100-idx-1)), end="")
			else:
				print("%d:\t%.3f\t%.3f"%(epoch, errors[-1], accuracies[-1]))

	# Produce plots
	fig, ax = plt.subplots(nrows = 2, ncols = 1)

	ax_errors = ax[0]
	ax_accuracy = ax[1]

	ax_errors.plot(range(0, eval_freq*len(errors), eval_freq), errors, label="test set")
	ax_errors.plot(range(0, eval_freq*len(errors_train), eval_freq), errors_train, label="train set")
	ax_errors.set_title("Losses over iterations")
	ax_errors.set_ylabel("loss")
	ax_errors.set_xlabel("step")
	ax_errors.legend()

	ax_accuracy.plot(range(0, eval_freq*len(accuracies), eval_freq), accuracies, label="test set")
	ax_accuracy.plot(range(0, eval_freq*len(accuracies_train), eval_freq), accuracies_train, label="train set")
	ax_accuracy.set_title("Accuracy over iterations")
	ax_accuracy.set_ylabel("accuracy")
	ax_accuracy.set_xlabel("step")
	ax_accuracy.legend()

	fig.tight_layout()
	fig.savefig("results_mlp_pytorch_optimized.png", dpi=900)

	########################
	# END OF YOUR CODE	#
	#######################

def print_flags():
	"""
	Prints all entries in FLAGS variable.
	"""
	for key, value in vars(FLAGS).items():
		print(key + ' : ' + str(value))

def main():
	"""
	Main function
	"""
	# Print all Flags to confirm parameter settings
	print_flags()

	if not os.path.exists(FLAGS.data_dir):
		os.makedirs(FLAGS.data_dir)

	# Run the training operation
	train()

if __name__ == '__main__':
	# Command line arguments
	parser = argparse.ArgumentParser()
	parser.add_argument('--dnn_hidden_units', type = str, default = DNN_HIDDEN_UNITS_DEFAULT,
						help='Comma separated list of number of units in each hidden layer')
	parser.add_argument('--learning_rate', type = float, default = LEARNING_RATE_DEFAULT,
						help='Learning rate')
	parser.add_argument('--max_steps', type = int, default = MAX_STEPS_DEFAULT,
						help='Number of steps to run trainer.')
	parser.add_argument('--batch_size', type = int, default = BATCH_SIZE_DEFAULT,
						help='Batch size to run trainer.')
	parser.add_argument('--eval_freq', type=int, default=EVAL_FREQ_DEFAULT,
						help='Frequency of evaluation on the test set')
	parser.add_argument('--data_dir', type = str, default = DATA_DIR_DEFAULT,
						help='Directory for storing input data')
	parser.add_argument('--neg_slope', type=float, default=NEG_SLOPE_DEFAULT,
						help='Negative slope parameter for LeakyReLU')
	parser.add_argument('--NGUI', type=bool, default=False,
						help='Print non fancy for running in cluster')
	FLAGS, unparsed = parser.parse_known_args()

	main()
