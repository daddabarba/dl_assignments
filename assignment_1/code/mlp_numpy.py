"""
This module implements a multi-layer perceptron (MLP) in NumPy.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from modules import * 

class MLP(object):
	"""
	This class implements a Multi-layer Perceptron in NumPy.
	It handles the different layers and parameters of the model.
	Once initialized an MLP object can perform forward and backward.
	"""

	def __init__(self, n_inputs, n_hidden, n_classes, neg_slope):
		"""
		Initializes MLP object. 
		
		Args:
		n_inputs: number of inputs.
		n_hidden: list of ints, specifies the number of units
				in each linear layer. If the list is empty, the MLP
				will not have any linear layers, and the model
				will simply perform a multinomial logistic regression.
		n_classes: number of classes of the classification problem.
				This number is required in order to specify the
				output dimensions of the MLP
		neg_slope: negative slope parameter for LeakyReLU
		
		TODO:
		Implement initialization of the network.
		"""

		########################
		# PUT YOUR CODE HERE  #
		#######################
		
		self.hidden_layers = []
		
		in_hidden = n_inputs
		
		# Initialize list of hidden layer
		for i in range(len(n_hidden)):
			out_hidden = n_hidden[i] # get output size of hidden layer
			self.hidden_layers.append({'Linear' : LinearModule(in_hidden, out_hidden), 'ReLU' : LeakyReLUModule(neg_slope)}) # each layer has a linear mapping and then applies ReLU
			in_hidden = out_hidden # output of previous layer is input of next layer
		
		# Initialize output layer
		self.output_layer = {'Linear' : LinearModule(in_hidden, n_classes), 'Softmax' : SoftMaxModule()}
		
		# Store number of hidden layers (for later use)
		self.n_hidden = len(n_hidden)
		
		########################
		# END OF YOUR CODE	  #
		#######################

	def forward(self, x):
		"""
		Performs forward pass of the input. Here an input tensor x is transformed through 
		several layer transformations.
		
		Args:
		x: input to the network
		Returns:
		out: outputs of the network
		
		TODO:
		Implement forward pass of the network.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		
		# Run input through each hidden layer (if any )
		
		for h in self.hidden_layers:
			x = h['ReLU'].forward( h['Linear'].forward(x) )
			
		# Compute final output
		out = self.output_layer['Softmax'].forward(self.output_layer['Linear'].forward(x))

		########################
		# END OF YOUR CODE	  #
		#######################

		return out

	def backward(self, dout):
		"""
		Performs backward pass given the gradients of the loss. 
		
		Args:
		dout: gradients of the loss
		
		TODO:
		Implement backward pass of the network.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		
		# Back-propagate first through output layer
		
		dx = self.output_layer['Linear'].backward(self.output_layer['Softmax'].backward(dout))
		
		# Back-propagate through each hidden layer (if any)
		
		for i in range(self.n_hidden):
			h = self.hidden_layers[self.n_hidden-1-i]
			dx = h['Linear'].backward( h['ReLU'].backward(dx) )
		
		########################
		# END OF YOUR CODE	  #
		#######################
		
		return
