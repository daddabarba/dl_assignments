"""
This module implements a Convolutional Neural Network in PyTorch.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
from torch import nn

class ConvNet(nn.Module):
	"""
	This class implements a Convolutional Neural Network in PyTorch.
	It handles the different layers and parameters of the model.
	Once initialized an ConvNet object can perform forward.
	"""

	def __init__(self, n_channels, n_classes):
		"""
		Initializes ConvNet object. 

		Args:
		n_channels: number of input channels
		n_classes: number of classes of the classification problem


		TODO:
		Implement initialization of the network.
		"""

		########################
		# PUT YOUR CODE HERE  #
		#######################

		super(ConvNet, self).__init__()

		self.layers = []

		self.layers.append(nn.Conv2d(n_channels, 64, 3, padding=1, stride=1))
		self.layers.append(nn.MaxPool2d(3, padding=1, stride=2))
		self.layers.append(nn.Conv2d(64, 128, 3, padding=1, stride=1))
		self.layers.append(nn.MaxPool2d(3, padding=1, stride=2))
		self.layers.append(nn.Conv2d(128, 256, 3, padding=1, stride=1))
		self.layers.append(nn.Conv2d(256, 256, 3, padding=1, stride=1))
		self.layers.append(nn.MaxPool2d(3, padding=1, stride=2))
		self.layers.append(nn.Conv2d(256, 512, 3, padding=1, stride=1))
		self.layers.append(nn.Conv2d(512, 512, 3, padding=1, stride=1))
		self.layers.append(nn.MaxPool2d(3, padding=1, stride=2))
		self.layers.append(nn.Conv2d(512, 512, 3, padding=1, stride=1))
		self.layers.append(nn.Conv2d(512, 512, 3, padding=1, stride=1))
		self.layers.append(nn.MaxPool2d(3, padding=1, stride=2))

		self.layers = nn.Sequential(*self.layers)
		self.output = nn.Linear(512, n_classes)

		########################
		# END OF YOUR CODE	  #
		#######################

	def forward(self, x):
		"""
		Performs forward pass of the input. Here an input tensor x is transformed through 
		several layer transformations.

		Args:
		x: input to the network
		Returns:
		out: outputs of the network

		TODO:
		Implement forward pass of the network.
		"""

		########################
		# PUT YOUR CODE HERE  #
		#######################
		out = self.output(torch.squeeze(self.layers(x)))
		########################
		# END OF YOUR CODE	  #
		#######################

		return out
