"""
This module implements various modules of the network.
You should fill in code into indicated sections.
"""
import numpy as np

class LinearModule(object):
	"""
	Linear module. Applies a linear transformation to the input data. 
	"""
	def __init__(self, in_features, out_features):
		"""
		Initializes the parameters of the module. 
		
		Args:
		in_features: size of each input sample
		out_features: size of each output sample

		TODO:
		Initialize weights self.params['weight'] using normal distribution with mean = 0 and 
		std = 0.0001. Initialize biases self.params['bias'] with 0. 
		
		Also, initialize gradients with zeros.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		self.params = {'weight': np.random.normal(0, 0.0001, [in_features, out_features]), 'bias': np.zeros([out_features])}
		self.grads = {'weight': np.zeros([in_features, out_features]), 'bias': np.zeros([out_features])}
		########################
		# END OF YOUR CODE	  #
		#######################

	def forward(self, x):
		"""
		Forward pass.
		
		Args:
		x: input to the module
		Returns:
		out: output of the module
		
		TODO:
		Implement forward pass of the module. 
		
		Hint: You can store intermediate variables inside the object. They can be used in backward pass computation.														   #
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		self.input = x
		out = x.dot(self.params['weight']) + self.params['bias']
		########################
		# END OF YOUR CODE	  #
		#######################

		return out

	def backward(self, dout):
		"""
		Backward pass.
		
		Args:
		dout: gradients of the previous module
		Returns:
		dx: gradients with respect to the input of the module
		
		TODO:
		Implement backward pass of the module. Store gradient of the loss with respect to 
		layer parameters in self.grads['weight'] and self.grads['bias']. 
		"""

		########################
		# PUT YOUR CODE HERE  #
		#######################

		# Turn vectors into 1D matrices

		if len(self.input.shape) < 2:
			self.input = self.input[None]

		if len(dout.shape) < 2:
			dout = dout[None]

		# Get batch_size
		batch_size = dout.shape[0]

		# Compute total (averaged) gradients for parameters
		self.grads = {'weight' : self.input.T.dot(dout), 'bias' : dout.sum(0)}

		# Compute gradient w.r.t. input (for each batch)
		dx = dout.dot(self.params['weight'].T)
		
		########################
		# END OF YOUR CODE	  #
		#######################
		
		return dx

class LeakyReLUModule(object):
	"""
	Leaky ReLU activation module.
	"""
	def __init__(self, neg_slope):
		"""
		Initializes the parameters of the module.
		
		Args:
		neg_slope: negative slope parameter.
		
		TODO:
		Initialize the module.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		self.neg_slope = neg_slope
		########################
		# END OF YOUR CODE	  #
		#######################

	def forward(self, x):
		"""
		Forward pass.
		
		Args:
		x: input to the module
		Returns:
		out: output of the module
		
		TODO:
		Implement forward pass of the module. 
		
		Hint: You can store intermediate variables inside the object. They can be used in backward pass computation.														   #
		"""

		########################
		# PUT YOUR CODE HERE  #
		#######################
		self.input = x
		out = np.maximum(0, x) + self.neg_slope*np.minimum(0,x)
		########################
		# END OF YOUR CODE	  #
		#######################
		
		return out

	def backward(self, dout):
		"""
		Backward pass.
		
		Args:
		dout: gradients of the previous module
		Returns:
		dx: gradients with respect to the input of the module
		
		TODO:
		Implement backward pass of the module.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		dx = np.ones(self.input.shape)
		dx[self.input<0] = self.neg_slope
		dx = dout*dx
		########################
		# END OF YOUR CODE	  #
		#######################
		
		return dx


class SoftMaxModule(object):
	"""
	Softmax activation module.
	"""
	
	def forward(self, x):
		"""
		Forward pass.
		Args:
		x: input to the module
		Returns:
		out: output of the module
		
		TODO:
		Implement forward pass of the module.
		To stabilize computation you should use the so-called Max Trick - https://timvieira.github.io/blog/post/2014/02/11/exp-normalize-trick/
		
		Hint: You can store intermediate variables inside the object. They can be used in backward pass computation.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		
		# Turn input in matrix form if vector
		if len(x.shape) < 2:
			x = x[None]
		
		exp_x = np.exp(x)
		denom = exp_x.sum(1)
		out = self.output = exp_x/denom[:, None]
		########################
		# END OF YOUR CODE	  #
		#######################
		
		return out
	
	def backward(self, dout):
		"""
		Backward pass.
		Args:
		dout: gradients of the previous modul
		Returns:
		dx: gradients with respect to the input of the module
		
		TODO:
		Implement backward pass of the module.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		
		# Transform dout in matrix form
		if len(dout.shape) < 2:
			dout = dout[None]
		
		# Transform output in matrix form
		if len(self.output) < 2:
			self.output = self.output[None]
		
		dx = self.output*(dout - (self.output*dout).sum(1)[:,None])
		
		#######################
		# END OF YOUR CODE	  #
		#######################
		
		return dx

class CrossEntropyModule(object):
	"""
	Cross entropy loss module.
	"""
	
	def forward(self, x, y):
		"""
		Forward pass.
		Args:
		x: input to the module
		y: labels of the input
		Returns:
		out: cross entropy loss
		
		TODO:
		Implement forward pass of the module.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		likelihood_targets = x[y>0]
		out = -np.log(likelihood_targets).sum()/x.shape[0]
		########################
		# END OF YOUR CODE	  #
		#######################
		
		return out
	
	def backward(self, x, y):
		"""
		Backward pass.
		Args:
		x: input to the module
		y: labels of the input
		Returns:
		dx: gradient of the loss with the respect to the input x.
		
		TODO:
		Implement backward pass of the module.
		"""
		
		########################
		# PUT YOUR CODE HERE  #
		#######################
		dx = np.zeros(x.shape)
		likelihood_targets = x[y>0]
		dx[y>0] = -1/(likelihood_targets*x.shape[0])
		########################
		# END OF YOUR CODE	  #
		#######################
		
		return dx
