"""
This module implements a multi-layer perceptron (MLP) in PyTorch.
You should fill in code into indicated sections.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from torch import nn

class MLP(nn.Module):
	"""
	This class implements a Multi-layer Perceptron in PyTorch.
	It handles the different layers and parameters of the model.
	Once initialized an MLP object can perform forward.
	"""

	def __init__(self, n_inputs, n_hidden, n_classes, neg_slope):
		"""
		Initializes MLP object.

		Args:
		n_inputs: number of inputs.
		n_hidden: list of ints, specifies the number of units
				in each linear layer. If the list is empty, the MLP
				will not have any linear layers, and the model
				will simply perform a multinomial logistic regression.
		n_classes: number of classes of the classification problem.
				This number is required in order to specify the
				output dimensions of the MLP
		neg_slope: negative slope parameter for LeakyReLU

		TODO:
		Implement initialization of the network.
		"""

		########################
		# PUT YOUR CODE HERE  #
		#######################

		super(MLP, self).__init__()

		self.layers = []

		in_hidden = n_inputs

		# Initialize list of hidden layer
		for i in range(len(n_hidden)):
			out_hidden = n_hidden[i] # get output size of hidden layer
			self.layers.append(nn.Linear(in_hidden, out_hidden))
			self.layers.append(nn.LeakyReLU(neg_slope))
			in_hidden = out_hidden # output of previous layer is input of next layer

		# Initialize output layer
		self.layers.append(nn.Linear(in_hidden, n_classes))
		# self.layers.append(nn.Softmax(dim=1))

		# Construct model that calls layers one after the other
		self.layers = nn.Sequential(*self.layers)

		# Store number of hidden layers (for later use)
		self.n_hidden = len(n_hidden)

		########################
		# END OF YOUR CODE	  #
		#######################

	def forward(self, x):
		"""
		Performs forward pass of the input. Here an input tensor x is transformed through 
		several layer transformations.

		Args:
		x: input to the network
		Returns:
		out: outputs of the network

		TODO:
		Implement forward pass of the network.
		"""

		########################
		# PUT YOUR CODE HERE  #
		#######################
		out = self.layers(x)
		########################
		# END OF YOUR CODE	  #
		#######################

		return out
