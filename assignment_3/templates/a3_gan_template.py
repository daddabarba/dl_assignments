import argparse
import os

import torch
import torch.nn as nn
import torchvision.transforms as transforms
from torchvision.utils import save_image
from torchvision import datasets

class Generator(nn.Module):
	def __init__(self, latent_dim):
		super(Generator, self).__init__()

		# Construct generator. You are free to experiment with your model,
		# but the following is a good start:
		#	Linear args.latent_dim -> 128
		#	LeakyReLU(0.2)
		#	Linear 128 -> 256
		#	Bnorm
		#	LeakyReLU(0.2)
		#	Linear 256 -> 512
		#	Bnorm
		#	LeakyReLU(0.2)
		#	Linear 512 -> 1024
		#	Bnorm
		#	LeakyReLU(0.2)
		#	Linear 1024 -> 768
		#	Output non-linearity

		self.gen = nn.Sequential(
			nn.Linear(latent_dim, 128),
			nn.LeakyReLU(0.2),
			nn.Linear(128, 256),
			nn.BatchNorm1d(256),
			nn.LeakyReLU(0.2),
			nn.Linear(256, 512),
			nn.BatchNorm1d(512),
			nn.LeakyReLU(0.2),
			nn.Linear(512, 1024),
			nn.BatchNorm1d(1024),
			nn.LeakyReLU(0.2),
			nn.Linear(1024, 784),
			nn.Tanh()
		)

	def forward(self, z):
		# Generate images from z
		return self.gen(z)



class Discriminator(nn.Module):
	def __init__(self):
		super(Discriminator, self).__init__()

		# Construct distriminator. You are free to experiment with your model,
		# but the following is a good start:
		#	Linear 784 -> 512
		#	LeakyReLU(0.2)
		#	Linear 512 -> 256
		#	LeakyReLU(0.2)
		#	Linear 256 -> 1
		#	Output non-linearity

		self.disc = nn.Sequential(
			nn.Linear(784, 512),
			nn.LeakyReLU(0.2),
			nn.Linear(512, 256),
			nn.LeakyReLU(0.2),
			nn.Linear(256, 1),
			nn.Sigmoid()
		)

	def forward(self, img):
		# return discriminator score for img
		return self.disc(img)


def train(dataloader, discriminator, generator, optimizer_G, optimizer_D):

	epochs_avg_loss_D = [0.0]*args.n_epochs
	epochs_avg_loss_G = [0.0]*args.n_epochs

	BCE = nn.BCELoss()

	for epoch in range(args.n_epochs):

		epoch_loss_D = 0.0
		epoch_loss_G = 0.0

		for i, (imgs, _) in enumerate(dataloader):

			batch_size = imgs.shape[0]

			# Set targets for BCE
			increase_chance = torch.ones(batch_size, 1).to(device)
			decrease_chance = torch.zeros(batch_size, 1).to(device)

			imgs = imgs.view(batch_size, -1).to(device)

			# Train Generator
			# ---------------

			# Generate latent noise
			z = torch.randn(batch_size, args.latent_dim).to(device)

			# Generate images
			fakes = generator(z)

			# Classify fakes
			score_fakes = discriminator(fakes)

			# Compute loss for generator
			l_G = BCE(score_fakes, increase_chance)

			# Store loss
			epoch_loss_G += l_G.item()

			# Train generator
			optimizer_G.zero_grad()
			l_G.backward()
			optimizer_G.step()


			# Train Discriminator
			# -------------------

			# Classify train images
			score_fakes = discriminator(fakes.detach())
			score_true = discriminator(imgs)

			# Compute loss for discriminator
			l_D = (BCE(score_fakes, decrease_chance) + BCE(score_true, increase_chance))/2

			# Store loss
			epoch_loss_D += l_D.item()

			# Train discriminator
			optimizer_D.zero_grad() # Also removes grads from l_G.backward()
			l_D.backward()
			optimizer_D.step()

			# Save Images
			# -----------
			batches_done = epoch * len(dataloader) + i
			if batches_done % args.save_interval == 0:
				# You can use the function save_image(Tensor (shape Bx1x28x28),
				# filename, number of rows, normalize) to save the generated
				# images, e.g.:
				gen_imgs = fakes.view(-1, 1, 28, 28)
				save_image(gen_imgs[:25],
							 'images/{}.png'.format(batches_done),
							 nrow=5, normalize=True)
				pass

			print("epoch %d (%d/%d): |%s| Disc: %.3f Gen: %.3f %s \r"%(epoch, i, len(dataloader), "▉"*int(i/len(dataloader)*100) + " "*int((1-i/len(dataloader))*100), epoch_loss_D/(i+1), epoch_loss_G/(i+1), " "*20), end="")

		# Store average losses per epoch
		epochs_avg_loss_D[epoch] = epoch_loss_D/len(dataloader)
		epochs_avg_loss_G[epoch] = epoch_loss_G/len(dataloader)

		# Print results
		print("epoch %d (%d/%d): |%s| Disc: %.3f Gen: %.3f"%(epoch, i, len(dataloader), "▉"*100, epochs_avg_loss_D[epoch], epochs_avg_loss_G[epoch]))


def main():
	# Create output image directory
	os.makedirs('images', exist_ok=True)

	# load data
	dataloader = torch.utils.data.DataLoader(
		datasets.MNIST('./data/mnist', train=True, download=True,
					   transform=transforms.Compose([
						   transforms.ToTensor(),
						   transforms.Normalize((0.5, ),
												(0.5, ))])),
		batch_size=args.batch_size, shuffle=True)

	# Initialize models and optimizers
	generator = Generator(args.latent_dim).to(device)
	discriminator = Discriminator().to(device)
	optimizer_G = torch.optim.Adam(generator.parameters(), lr=args.lr)
	optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=args.lr)

	# Start training
	train(dataloader, discriminator, generator, optimizer_G, optimizer_D)

	# You can save your generator here to re-use it to generate images for your
	# report, e.g.:
	torch.save(generator.state_dict(), "mnist_generator.pt")

	# Interpolation in latent space



if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--n_epochs', type=int, default=200,
						help='number of epochs')
	parser.add_argument('--batch_size', type=int, default=64,
						help='batch size')
	parser.add_argument('--lr', type=float, default=0.0002,
						help='learning rate')
	parser.add_argument('--latent_dim', type=int, default=100,
						help='dimensionality of the latent space')
	parser.add_argument('--save_interval', type=int, default=500,
						help='save every SAVE_INTERVAL iterations')
	parser.add_argument('--device', type=str, default="cuda:0",
						help='device on which to run model')
	parser.add_argument('--max_norm', type=float, default=10.0)

	args = parser.parse_args()

	device = torch.device(args.device)

	main()
