import argparse

import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
from torchvision.utils import make_grid

from datasets.bmnist import bmnist


class Encoder(nn.Module):

	def __init__(self, input_dim, hidden_dim=500, z_dim=20):
		super().__init__()

		self.projection_dpars = nn.Sequential(
			nn.Linear(input_dim, hidden_dim),
			nn.ReLU(),
			nn.Linear(hidden_dim, 2*z_dim)
		)

	def forward(self, input):
		"""
		Perform forward pass of encoder.

		Returns mean and std with shape [batch_size, z_dim]. Make sure
		that any constraints are enforced.
		"""

		mean, log_std = torch.chunk(self.projection_dpars(input), 2, -1)

		return mean, torch.exp(log_std)


class Decoder(nn.Module):

	def __init__(self, input_dim, hidden_dim=500, z_dim=20):
		super().__init__()

		self.decode = nn.Sequential(
			nn.Linear(z_dim, hidden_dim),
			nn.Tanh(),
			nn.Linear(hidden_dim, input_dim),
			nn.Sigmoid()
		)

	def forward(self, input):
		"""
		Perform forward pass of encoder.

		Returns mean with shape [batch_size, 784].
		"""
		mean = self.decode(input)

		return mean


class VAE(nn.Module):

	def __init__(self, input_dim, hidden_dim=500, z_dim=20):
		super().__init__()

		self.z_dim = z_dim
		self.encoder = Encoder(input_dim, hidden_dim, z_dim)
		self.decoder = Decoder(input_dim, hidden_dim, z_dim)

		self.device = torch.device("cpu")

	def to(self, *args, **kwargs):

		ret = super(VAE, self).to(*args, **kwargs)

		if isinstance(args[0], str) or isinstance(args[0], torch.device):
			self.device = args[0]

		return ret

	def forward(self, input):
		"""
		Given input, perform an encoding and decoding step and return the
		negative average elbo for the given batch.
		"""

		# Encode (infer distribution over latent variables)
		mean, std = self.encoder(input)

		# Sample noise
		epsilon = torch.randn(input.shape[0],self.z_dim).to(self.device)

		# Compute sampled latent variables
		z = mean + std*epsilon

		# Reconstruct input from latent variables
		recon = self.decoder(z)

		# Compute reconstruction loss
		L_recon = (input * (recon + 1e-10).log() + (1-input) * (1-recon + 1e-10).log()).sum(1)

		# Compute regularization loss
		L_reg = 0.5 * (1 + 2*(std + 1e-10).log() - std**2 - mean**2).sum(1)

		average_negative_elbo = -torch.mean(L_reg + L_recon)
		return average_negative_elbo

	def sample(self, n_samples):
		"""
		Sample n_samples from the model. Return both the sampled images
		(from bernoulli) and the means for these bernoullis (as these are
		used to plot the data manifold).
		"""

		# Generate encodings
		if isinstance(n_samples, int):
			z = torch.randn(n_samples, self.z_dim).to(self.device)
		elif isinstance(n_samples, torch.Tensor):
			z = n_samples

		# Decode z vectors
		recon = self.decoder(z).cpu()

		sampled_ims, im_means = torch.bernoulli(recon), recon

		return sampled_ims, im_means


def epoch_iter(model, data, optimizer):
	"""
	Perform a single epoch for either the training or validation.
	use model.training to determine if in 'training mode' or not.

	Returns the average elbo for the complete epoch.
	"""

	average_epoch_elbo = 0

	for batch in data:

		# Preprocess input
		x = batch.reshape(batch.shape[0], -1).to(ARGS.device)

		# If in train mode, perform backprop
		if model.training:

			# Run batch in model
			epoch_elbo = model(x)

			# Reset gradients
			optimizer.zero_grad()

			# Compute gradients
			epoch_elbo.backward()

			# Run optimization step
			optimizer.step()
		else:
			with torch.no_grad():
				epoch_elbo = model(x)

		average_epoch_elbo	+= epoch_elbo

	average_epoch_elbo /= len(data)

	return average_epoch_elbo


def run_epoch(model, data, optimizer):
	"""
	Run a train and validation epoch and return average elbo for each.
	"""
	traindata, valdata = data

	model.train()
	train_elbo = epoch_iter(model, traindata, optimizer)

	model.eval()
	val_elbo = epoch_iter(model, valdata, optimizer)

	return train_elbo, val_elbo


def show_elbo_plot(train_curve, val_curve ):
	plt.figure(figsize=(12, 6))
	plt.plot(train_curve, label='train elbo')
	plt.plot(val_curve, label='validation elbo')
	plt.legend()
	plt.xlabel('epochs')
	plt.ylabel('ELBO')
	plt.tight_layout()
	plt.show()

def main():

	device = torch.device(ARGS.device)

	data = bmnist()[:2]	 # ignore test split
	model = VAE(784, z_dim=ARGS.zdim).to(device)
	optimizer = torch.optim.Adam(model.parameters())

	num_samples = ARGS.num_samples

	# plt.ion()

	train_curve, val_curve = [], []
	for epoch in range(ARGS.epochs):

		with torch.no_grad():
			samples, _ = model.sample(num_samples**2)

			samples = samples.view(-1, 1, 28, 28)
			grid = make_grid(samples, nrow=num_samples)

			plt.imshow(grid.detach().numpy().transpose(1,2,0))
			plt.draw()
			plt.pause(0.05)

			if epoch == 0 or epoch == int(ARGS.epochs/2) or epoch == (ARGS.epochs-1):
				plt.savefig("vae_samples_%d.png"%(epoch), dsi=900)

		elbos = run_epoch(model, data, optimizer)
		train_elbo, val_elbo = elbos
		train_curve.append(train_elbo)
		val_curve.append(val_elbo)
		print(f"[Epoch {epoch}] train elbo: {train_elbo} val_elbo: {val_elbo}")

		# --------------------------------------------------------------------
		#  Add functionality to plot samples from model during training.
		#  You can use the make_grid functioanlity that is already imported.
		# --------------------------------------------------------------------


	# --------------------------------------------------------------------
	#  Add functionality to plot plot the learned data manifold after
	#  if required (i.e., if zdim == 2). You can use the make_grid
	#  functionality that is already imported.
	# --------------------------------------------------------------------

	if ARGS.zdim == 2:
		with torch.no_grad():

			n = ARGS.manifold_size

			# Bin domain of z
			z1_bins = torch.linspace(-1,1, n)
			z2_bins = torch.linspace(-1,1, n)

			# Get mesh of values
			z1, z2 = torch.ones(n,1).mm(z1_bins[None]).flatten(), z2_bins[:, None].mm(torch.ones(1,n)).flatten()

			# Construct batch of z vectors
			z = torch.stack([z1, z2], dim=1)

			# Sample
			_, samples = model.sample(z.to(model.device))

			# Plot batch
			samples = samples.view(-1, 1, 28, 28)
			grid = make_grid(samples, nrow=n)

			plt.figure()
			plt.imshow(grid.detach().numpy().transpose(1,2,0))
			plt.draw()



	show_elbo_plot(train_curve, val_curve)


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--epochs', default=40, type=int,
						help='max number of epochs')
	parser.add_argument('--zdim', default=20, type=int,
						help='dimensionality of latent space')
	parser.add_argument('--num_samples', default=10, type=int,
						help=' (Square root) of how many images to sample from the model at each epoch')
	parser.add_argument('--manifold_size', default=20, type=int,
						help=' (Square root) of how many bins to use for z\'s domain when plotting the manifold')
	parser.add_argument('--device', default="cuda:0", type=str,
						help='torch device to be used')

	ARGS = parser.parse_args()

	main()
