import torch
import argparse

from a3_gan_template import Generator

from torchvision.utils import make_grid
from torchvision.utils import save_image
import matplotlib.pyplot as plt

def main():
	generator = Generator(args.latent_dim).to(device)

	generator.load_state_dict(torch.load("mnist_generator.pt"))


	with torch.no_grad():

		# Sample two (latent) images
		z_1, z_2 = torch.randn(2, args.latent_dim).chunk(2, dim=0)

		# Get interpolating factors
		alpha = torch.arange(0, 1+1e-10, 1/8)[:, None]

		# Compute all interpolations (including original images)
		interp = alpha*z_1 + (1-alpha)*z_2

		# Generate images
		images = generator(interp.to(device)).cpu()

		# Show images

		images = images.view(-1, 1, 28, 28)
		# grid = make_grid(images, nrow=1)

		save_image(images,
				   'interpolations.png',
				   nrow=9, normalize=True)
		# plt.imshow(grid.detach().numpy().transpose(1,2,0))
		# plt.show()

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--n_epochs', type=int, default=200,
						help='number of epochs')
	parser.add_argument('--batch_size', type=int, default=64,
						help='batch size')
	parser.add_argument('--lr', type=float, default=0.0002,
						help='learning rate')
	parser.add_argument('--latent_dim', type=int, default=100,
						help='dimensionality of the latent space')
	parser.add_argument('--save_interval', type=int, default=500,
						help='save every SAVE_INTERVAL iterations')
	parser.add_argument('--device', type=str, default="cuda:0",
						help='device on which to run model')
	parser.add_argument('--max_norm', type=float, default=10.0)

	args = parser.parse_args()

	device = torch.device(args.device)

	main()
