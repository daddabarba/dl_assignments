# MIT License
#
# Copyright (c) 2019 Tom Runia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import numpy as np

import torch.nn as nn
from torch.nn import Parameter

from torch.nn import LSTM
from torch.nn import Linear


class TextGenerationModel(nn.Module):

	def __init__(self, batch_size, seq_length, vocabulary_size,
				 lstm_num_hidden=256, lstm_num_layers=2, device='cuda:0'):

		super(TextGenerationModel, self).__init__()

		# Store fields
		self.batch_size = batch_size
		self.seq_length = seq_length
		self.vocabulary_size = vocabulary_size

		self.device = device

		# Init model using LSTM
		self.lstm = LSTM(vocabulary_size, lstm_num_hidden, lstm_num_layers, batch_first = True)

		# Init parameters of output layer
		self.linear = Linear(lstm_num_hidden, vocabulary_size)

		self.to(device)


	def forward(self, x, in_state=None):

		assert (x.shape[2] == self.vocabulary_size), "Expected input of size %d, instead got %d"%(self.vocabulary_size, x.shape[2])

		# Get lstm output of shape TIME x BATCH x HIDDEN_SIZE
		lstm_out, state = self.lstm(x, in_state)

		# Compute output for each time step and batch
		return self.linear(lstm_out), state

	def complete_sentence(self, cue, seq_length, temp):

		# Get batch indices
		batch_ix = np.arange(cue.shape[0])

		# Initialize total output tensor
		completion = torch.zeros(cue.shape[0], seq_length, self.vocabulary_size).to(self.device)
		next_char = torch.zeros(cue.shape).to(self.device)

		# Set initial cue
		completion[:, 0:cue.shape[1], :] = cue
		next_char[:, 0:cue.shape[1], :] = cue

		# Init state
		state = None

		for t in range(cue.shape[1], seq_length):

			out, state = self.forward(next_char, state)

			# Get chosen characters
			if temp <= 0:
				ixs = out[:,-1,:].argmax(-1)
			else:
				p_dist = torch.softmax(temp*out[:, -1, :], dim=-1)
				ixs = torch.multinomial(p_dist, 1).flatten()

			# Store character
			completion[batch_ix, t, ixs] = 1

			next_char = torch.zeros(cue.shape[0], 1, cue.shape[2]).to(self.device)
			next_char[batch_ix,0, ixs] = 1
		return completion


