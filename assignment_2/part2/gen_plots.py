'''
This script, given a folder with slurm logs as arguments, generates the plots for exercise 1 of this assignment.
The last row of each log should be a string in json format that contains all the results and parameter used in that
specific experiment. Losses and accuracies are averaged over different random seeds (used in the experiment). The averaged
loss and accuracy (over different random seeds) is shown over train-steps, for each model. The max accuracy and min loss
for each model (w.r.t. the palindrome length) is also plot.
'''



import matplotlib.pyplot as plt
import numpy as np

import os
import sys

import json
import collections

import subprocess as sp


data_file = sys.argv[1]
print("Extracting data from ", data_file, "...", end="")

# Get last line
last_line = sp.check_output(["tail", "-n", "1", data_file]).decode("UTF-8")[0:-1]

# Parse JSON from string
data = json.loads(last_line)

print(" done")

print("Plotting data")

fig = plt.figure()

ax_acc, ax_loss = fig.subplots(2,1)

ax_acc.plot(data["accuracies"])
ax_loss.plot(data["losses"])

ax_acc.set_xlabel("Training step")
ax_acc.set_ylabel("Accuracy")
ax_acc.set_title("Accuracy during training for generative LSTM")

ax_loss.set_xlabel("Training step")
ax_loss.set_ylabel("Loss")
ax_loss.set_title("Loss during training for generative LSTM")

# fig_tot.savefig("results_tot.png", dpi=900)
fig.tight_layout()

plt.show()

