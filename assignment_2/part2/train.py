# MIT License
#
# Copyright (c) 2019 Tom Runia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.append('.')

import sys
sys.path.append('..')

import pickle

import os
import time
from datetime import datetime
import argparse

import numpy as np

from random import randint

import torch
import torch.optim as optim
from torch.utils.data import DataLoader

from part2.dataset import TextDataset
from part2.model import TextGenerationModel

################################################################################

def train(config):

	# Initialize the device which to run the model on
	device = torch.device(config.device)

	# Initialize the dataset and data loader (note the +1)
	dataset = TextDataset(config.txt_file, config.seq_length)
	data_loader = DataLoader(dataset, config.batch_size, num_workers=1)
	vocab_size = dataset.vocab_size

	# Initialize the model that we are going to use
	model = TextGenerationModel(config.batch_size, config.seq_length, vocab_size, config.lstm_num_hidden, config.lstm_num_layers, device)

	# Setup the loss and optimizer
	criterion = torch.nn.CrossEntropyLoss().to(device)
	optimizer = torch.optim.RMSprop(model.parameters(), config.learning_rate)

	# Init loss and accuracy lists
	losses = []
	accuracies = []

	# Open output file
	f_gen_sen = open(config.gen_sen_path, 'w')

	step = 0
	while step < config.train_steps:

		for batch_inputs, batch_targets in data_loader:

			# Send target to device once
			t = batch_targets.to(device)

			# Only for time measurement of step through network
			t1 = time.time()

			x = torch.nn.functional.one_hot(batch_inputs.to(torch.int64), num_classes = vocab_size).float()
			p, _ = model(x.to(device))

			loss = criterion(torch.flatten(p, 0, 1), torch.flatten(t))
			accuracy = torch.mean((torch.argmax(p, dim=2) == t).to(torch.float64))

			optimizer.zero_grad()
			loss.backward()

			optimizer.step()

			losses.append(float(loss.cpu().detach().numpy()))
			accuracies.append(float(accuracy.cpu().detach().numpy()))

			# Just for time measurement
			t2 = time.time()
			examples_per_second = config.batch_size/float(t2-t1)

			if step % config.print_every == 0:

				print("[{}] Train Step {:04d}/{:04d}, Batch Size = {}, Examples/Sec = {:.2f}, "
					  "Accuracy = {:.2f}, Loss = {:.3f}".format(
						  datetime.now().strftime("%Y-%m-%d %H:%M"), step,
						  int(config.train_steps), config.batch_size, examples_per_second,
						  accuracy, loss
					  ))

			if step % config.sample_every == 0:

				with torch.no_grad():
					# Construct input with hints (first random character)
					cue = torch.zeros([config.batch_size, 1, vocab_size])

					for b in range(config.batch_size):
						cue[b,0, randint(0, vocab_size-1)] = 1 # one-hot encode random word for the first time-step

					# Make the network run
					gen_onehot = model.complete_sentence(cue.to(device), config.seq_length, config.temp)

					# Convert to index lists
					cue_ix = cue.cpu().argmax(-1).detach().numpy()

					# Convert to index lists
					gen_ix = gen_onehot.cpu().argmax(-1).detach().numpy()

					for b in range(config.batch_size):

						# Convert to strings
						cue_str = dataset.convert_to_string(cue_ix[b])

						# Convert to strings
						gen_str = dataset.convert_to_string(gen_ix[b])

						f_gen_sen.write("[%d/%d]\t"%(step, config.train_steps) + gen_str + "\n")

			step += 1

			if step == config.train_steps:
				# If you receive a PyTorch data-loader error, check this bug report:
				# https://github.com/pytorch/pytorch/pull/9655
				break

	print('Done training.')

	f_gen_sen.close()

	if config.save_model_in is not None:
		print("Saving model in ", config.save_model_in)
		torch.save(model, config.save_model_in)

	if config.save_data_in is not None:

		with open(config.save_data_in, 'wb') as f:
			pickle.dump(dataset, f)

	# Print results (with parameters) in json format
	pars = "{\'accuracies\' : " + str(accuracies) +	 ", \'losses\' : " + str(losses) + ", \'parameters\' : " + str(config.__dict__) + "}"
	print(pars.replace("\'", "\"").replace("None", "\"None\""))

 ################################################################################
 ################################################################################

if __name__ == "__main__":

	# Parse training configuration
	parser = argparse.ArgumentParser()

	# Model params
	parser.add_argument('--txt_file', type=str, required=True, help="Path to a .txt file to train on")
	parser.add_argument('--seq_length', type=int, default=30, help='Length of an input sequence')
	parser.add_argument('--lstm_num_hidden', type=int, default=128, help='Number of hidden units in the LSTM')
	parser.add_argument('--lstm_num_layers', type=int, default=2, help='Number of LSTM layers in the model')
	parser.add_argument('--device', type=str, default="cuda:0", help="Training device 'cpu' or 'cuda:0'")
	parser.add_argument('--temp', type=float, default=-1, help="Softmax temp (don't set to use greedy sentence generation)")

	# Training params
	parser.add_argument('--batch_size', type=int, default=64, help='Number of examples to process in a batch')
	parser.add_argument('--learning_rate', type=float, default=2e-3, help='Learning rate')

	# It is not necessary to implement the following three params, but it may help training.
	parser.add_argument('--learning_rate_decay', type=float, default=0.96, help='Learning rate decay fraction')
	parser.add_argument('--learning_rate_step', type=int, default=5000, help='Learning rate step')
	parser.add_argument('--dropout_keep_prob', type=float, default=1.0, help='Dropout keep probability')

	parser.add_argument('--train_steps', type=int, default=1e6, help='Number of training steps')
	parser.add_argument('--max_norm', type=float, default=5.0, help='--')

	# Misc params
	parser.add_argument('--summary_path', type=str, default="./summaries/", help='Output path for summaries')
	parser.add_argument('--gen_sen_path', type=str, default="./gen_sentences.txt", help='Output path for genereated sentences')
	parser.add_argument('--print_every', type=int, default=5, help='How often to print training progress')
	parser.add_argument('--sample_every', type=int, default=100, help='How often to sample from the model')
	parser.add_argument('--save_model_in', type=str, default=None, help='File in which to store network\' s model')
	parser.add_argument('--save_data_in', type=str, default=None, help='File in which to serialize dataset')

	config = parser.parse_args()

	# Train the model
	train(config)
