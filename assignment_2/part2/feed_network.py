
# MIT License
#
# Copyright (c) 2019 Tom Runia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.append('..')

import pickle

import os
import argparse

import numpy as np
import torch

from part2.dataset import TextDataset
from part2.model import TextGenerationModel

################################################################################
################################################################################


def feed_net(config):

	# Import model
	model = torch.load(config.load_model_from)
	model.eval()

	# Load data-set
	with open(config.load_data_from, 'rb') as f:
		dataset = pickle.load(f)

	# Feed network
	feed = None
	while feed is None or feed != config.stop_word:

		# Get values
		feed = input("Feed network: ")

		if feed == config.stop_word:
			break

		seq_length = int(input("Run for: "))

		# Convert to indexes
		feed_ix = list(map(dataset._char_to_ix.get, list(feed)))

		# Convert indices to input cue
		cue = torch.nn.functional.one_hot(torch.Tensor(feed_ix)[None,:].to(torch.int64), num_classes=dataset.vocab_size)

		# Run cue in network
		gen_onehot = model.complete_sentence(cue, seq_length, config.temp)

		# Convert to index lists
		gen_ix = gen_onehot.argmax(-1).cpu().detach().numpy()

		# Convert to strings
		gen_str = dataset.convert_to_string(gen_ix[0])

		print("Out: ", gen_str)


if __name__ == "__main__":

	# Parse training configuration
	parser = argparse.ArgumentParser()

	# Model params
	parser.add_argument('--device', type=str, default="cpu", help="Training device 'cpu' or 'cuda:0'")
	parser.add_argument('--seq_length', type=int, default=30, help='Length of an input sequence')
	parser.add_argument('--temp', type=float, default=-1, help="Softmax temp (don't set to use greedy sentence generation)")

	# Misc params
	parser.add_argument('--stop_word', type=str, default="exit", help='Input that stops script from running')
	parser.add_argument('--load_model_from', type=str, required=True, help='File from which to load network\' s model')
	parser.add_argument('--load_data_from', type=str, required=True, help='File from which to load data')

	config = parser.parse_args()

	# Train the model
	feed_net(config)
