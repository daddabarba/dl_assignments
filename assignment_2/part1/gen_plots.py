'''
This script, given a folder with slurm logs as arguments, generates the plots for exercise 1 of this assignment.
The last row of each log should be a string in json format that contains all the results and parameter used in that
specific experiment. Losses and accuracies are averaged over different random seeds (used in the experiment). The averaged
loss and accuracy (over different random seeds) is shown over train-steps, for each model. The max accuracy and min loss
for each model (w.r.t. the palindrome length) is also plot.
'''



import matplotlib.pyplot as plt
import numpy as np

import os
import sys

import json
import collections

import subprocess as sp


data_dir = sys.argv[1]
print("Extracting data from ", data_dir)

logs = []

for file_name in os.listdir(data_dir):

	# Get file name
	full_path = os.path.join(data_dir, file_name)
	print("\textracting log ", full_path, "...", end="")

	# Get last line
	last_line = sp.check_output(["tail", "-n", "1", full_path]).decode("UTF-8")[0:-1]
	# Parse JSON from string
	logs.append(json.loads(last_line))

	print(" done")


# Reorganize data dictionary
print("\nRepackaging data...", end="")

data = {}

for log in logs:

	# Store per model
	model = log["parameters"]["model_type"]

	if model not in data.keys():
		data[model] = {}

	# Store per palindrome length
	palindrome_length = log["parameters"]["input_length"]

	if palindrome_length not in data[model].keys():
		expected_length = log["parameters"]["train_steps"] + 1
		data[model][palindrome_length] = {"accuracies" : np.zeros(expected_length),
										  "losses" : np.zeros(expected_length),
										  "N" : 0, "avg_acc" : 0.0, "avg_loss" : 0.0}

	# Store accuracies and losses
	data[model][palindrome_length]["accuracies"] += np.array(log["accuracies"])
	data[model][palindrome_length]["losses"] += np.array(log["losses"])

	data[model][palindrome_length]["N"] += 1

	data[model][palindrome_length]["avg_acc"] += log["avg_acc"]
	data[model][palindrome_length]["avg_loss"] += log["avg_loss"]


print(" done")

# Average accuracy and loss values
print("Averaging results over random seeds...", end="")

for model_data in data.values():
	for length_data in model_data.values():
		length_data["accuracies"] /= length_data["N"]
		length_data["losses"] /= length_data["N"]

		length_data["max_acc"] = length_data["avg_acc"]/length_data["N"]
		length_data["min_loss"] = length_data["avg_loss"]/length_data["N"]

print(" done")

# Plotting
print("Plotting data")

fig_tot = plt.figure(len(data.keys()))

ax_acc_total, ax_loss_total = fig_tot.subplots(2,1)


# Plot results with time, length, and model
for i, (model, model_data) in enumerate(data.items()):

	print("\tPlotting data for model ", model)

	fig = plt.figure(i)
	ax_acc, ax_loss = fig.subplots(2,1)

	model_data_sorted = collections.OrderedDict(sorted(model_data.items()))

	acc_per_length = []
	loss_per_length = []

	skip = int(input("Skip every (-1 to not skip): "))

	for i_l, (length, length_data) in enumerate(model_data_sorted.items()):

		accuracies = length_data["accuracies"]
		losses = length_data["losses"]

		x = list(range(len(accuracies)))

		acc_per_length.append(length_data["max_acc"])
		loss_per_length.append(length_data["min_loss"])

		#Skip every n
		if skip>1 and (i_l%skip)==0:
			continue

		print("\t\tPlotting data for length ", length, "...", end="")

		ax_acc.plot(x, accuracies, label=("length=" + str(length)))
		ax_loss.plot(x, losses, label=("length=" + str(length)))


		print(" done")

	ax_acc.set_xlabel("Train-steps")
	ax_acc.set_ylabel("Average accuracy")
	ax_acc.set_title("Average accuracy for " + str(model))

	ax_loss.set_xlabel("Train-steps")
	ax_loss.set_ylabel("Average loss")
	ax_loss.set_title("Average loss for " + str(model))

	ax_acc.legend()
	ax_loss.legend()

	# Plot results for length and model

	ax_acc_total.plot(list(model_data_sorted.keys()), acc_per_length, label=model)
	ax_loss_total.plot(list(model_data_sorted.keys()), loss_per_length, label=model)

	# Save fig per model
	# fig.savefig("results_" + model + ".png", dpi=900)
	fig.tight_layout()

ax_acc_total.set_xlabel("Palindrome length")
ax_acc_total.set_ylabel("Max accuracy")
ax_acc_total.set_title("Max accuracy wrt palindrome length per model")

ax_loss_total.set_xlabel("Palindrome length")
ax_loss_total.set_ylabel("Min loss")
ax_loss_total.set_title("Min loss wrt palindrome length per model")

ax_acc_total.legend()
ax_loss_total.legend()

# fig_tot.savefig("results_tot.png", dpi=900)
fig_tot.tight_layout()

plt.show()

