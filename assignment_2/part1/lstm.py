################################################################################
# MIT License
#
# Copyright (c) 2019
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn

################################################################################


class LSTM(nn.Module):

	def __init__(self, seq_length, input_dim, num_hidden, num_classes, device='cpu', requires_grad=False ):
		super(LSTM, self).__init__()

		# Store parameters needed
		self.seq_length = seq_length
		self.num_hidden = num_hidden

		self.requires_grad = requires_grad

		# Init model's trainable parameters

		# Modulation gate
		self.W_gx = nn.Parameter(torch.randn([input_dim, num_hidden]))
		self.W_gh = nn.Parameter(torch.randn([num_hidden, num_hidden]))
		self.b_g = nn.Parameter(torch.zeros([1, num_hidden]))

		# Input gate
		self.W_ix = nn.Parameter(torch.randn([input_dim, num_hidden]))
		self.W_ih = nn.Parameter(torch.randn([num_hidden, num_hidden]))
		self.b_i = nn.Parameter(torch.zeros([1, num_hidden]))

		# Forget gate
		self.W_fx = nn.Parameter(torch.randn([input_dim, num_hidden]))
		self.W_fh = nn.Parameter(torch.randn([num_hidden, num_hidden]))
		self.b_f = nn.Parameter(torch.zeros([1, num_hidden]))

		# Output gate
		self.W_ox = nn.Parameter(torch.randn([input_dim, num_hidden]))
		self.W_oh = nn.Parameter(torch.randn([num_hidden, num_hidden]))
		self.b_o = nn.Parameter(torch.zeros([1, num_hidden]))

		# Output layer
		self.W_ph = nn.Parameter(torch.randn([num_hidden, num_classes]))
		self.b_p = nn.Parameter(torch.zeros([1, num_classes]))

		# Run xavier init
		# torch.nn.init.xavier_uniform_(self.W_gx)
		# torch.nn.init.xavier_uniform_(self.W_gh)

		# torch.nn.init.xavier_uniform_(self.W_ix)
		# torch.nn.init.xavier_uniform_(self.W_ih)

		# torch.nn.init.xavier_uniform_(self.W_fx)
		# torch.nn.init.xavier_uniform_(self.W_fh)

		# torch.nn.init.xavier_uniform_(self.W_ox)
		# torch.nn.init.xavier_uniform_(self.W_oh)

		# torch.nn.init.xavier_uniform_(self.W_ph)

		# Initial values for recurrent vectors
		self.c_init = torch.zeros([1, self.num_hidden], requires_grad = False).to(device)
		self.h_init = torch.zeros([1, self.num_hidden], requires_grad = True).to(device)

		# Init activation functions
		self.tanh = nn.Tanh().to(device)
		self.sigm = nn.Sigmoid().to(device)

		self.to(device)

	def forward(self, x):
		# We assume x to be BATCH_SIZE x TIME x INPUT_SIZE

		assert (x.shape[1] == self.seq_length), "Expected sequence of length %d instead got %d"%(self.seq_length, x.shape[1])
		assert (x.shape[2] == self.W_ix.shape[0]), "Expected input vector of size %d instead got %d"%(self.W_hx.shape[0], x.shape[2])

		batch_size = x.shape[0]

		# Initialize output matrix
		p = torch.zeros([batch_size, self.seq_length, self.b_p.shape[-1]])

		# Init h value

		h = self.h_init
		c = self.c_init

		if self.requires_grad:
			self.h_t = []

			h.requires_grad_(True)
			h.retain_grad()

			self.h_t.append(h)

		# Loop through each time-step
		for t in range(self.seq_length):

			# Get input x_t (as batch_size x input_size matrix) for each batch
			x_t = x[:, t, :]

			# Compute modulation gate value
			g = self.tanh( x_t.mm(self.W_gx) + h.mm(self.W_gh) + self.b_g )

			# Compute input, forget, and output gate values
			i = self.sigm( x_t.mm(self.W_ix) + h.mm(self.W_ih) + self.b_i )
			f = self.sigm( x_t.mm(self.W_fx) + h.mm(self.W_fh) + self.b_f )
			o = self.sigm( x_t.mm(self.W_ox) + h.mm(self.W_oh) + self.b_o )

			# Update cell state value
			c = g*i + c*f

			# Update hidden layer activation
			h = self.tanh(c)*o

			if self.requires_grad:
				h.requires_grad_(True)
				h.retain_grad()

				self.h_t.append(h)

			# Compute output for current time-step
			p[:,t,:] = h.mm(self.W_ph) + self.b_p

		return p

