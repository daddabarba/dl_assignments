################################################################################
# MIT License
#
# Copyright (c) 2019
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn

################################################################################

class VanillaRNN(nn.Module):

	def __init__(self, seq_length, input_dim, num_hidden, num_classes, device='cpu', requires_grad=False):
		super(VanillaRNN, self).__init__()

		self.seq_length = seq_length
		self.num_hidden = num_hidden

		self.requires_grad = requires_grad

		self.device = device

		# Initialize weight matrices (and bias) for hidden layer
		self.W_hx = nn.Parameter(torch.randn([input_dim, num_hidden]))
		self.W_hh = nn.Parameter(torch.randn([num_hidden, num_hidden]))

		self.b_h = nn.Parameter(torch.zeros([1, num_hidden]))

		# Initialize activation function
		self.tanh = nn.Tanh().to(device)

		# Initialize weight matrix (and bias) for output vector
		self.W_ph = nn.Parameter(torch.randn([num_hidden, num_classes]))
		self.b_p = nn.Parameter(torch.zeros([1, num_classes]))

		# Run xavier init
		# torch.nn.init.xavier_uniform_(self.W_hx)
		# torch.nn.init.xavier_uniform_(self.W_hh)
		# torch.nn.init.xavier_uniform_(self.W_ph)

		# Initialize state of hidden layer
		self.h_init = torch.zeros([1, self.num_hidden], requires_grad = True).to(device)

		self.to(device)

	def forward(self, x):
		# We assume x to be BATCH_SIZE x TIME x INPUT_SIZE

		assert (x.shape[1] == self.seq_length), "Expected sequence of length %d instead got %d"%(self.seq_length, x.shape[1])
		assert (x.shape[2] == self.W_hx.shape[0]), "Expected input vector of size %d instead got %d"%(self.W_hx.shape[0], x.shape[2])

		batch_size = x.shape[0]

		# Initialize output matrix
		p = torch.zeros([batch_size, self.seq_length, self.b_p.shape[-1]])

		# Init list of hidden states
		h = self.h_init

		if self.requires_grad:
			self.h_t = []

			h.requires_grad_(True)
			h.retain_grad()

			self.h_t.append(h)

		# Loop through each time-step
		for t in range(self.seq_length):

			# Get input x_t (as batch_size x input_size matrix) for each batch
			x_t = x[:, t, :]

			h = self.tanh( x_t.mm(self.W_hx) + h.mm(self.W_hh)	+ self.b_h)

			if self.requires_grad:
				h.requires_grad_(True)
				h.retain_grad()

				self.h_t.append(h)

			p[:, t, :] = h.mm(self.W_ph) + self.b_p

		return p
