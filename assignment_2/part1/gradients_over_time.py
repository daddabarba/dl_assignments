################################################################################
# MIT License
#
# Copyright (c) 2019
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to conditions.
#
# Author: Deep Learning Course | Fall 2019
# Date Created: 2019-09-06
################################################################################

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.append('..')
sys.path.append('.')

import matplotlib.pyplot as plt

import argparse

from torch.utils.data import DataLoader
from part1.dataset import PalindromeDataset

import torch
import numpy as np

import torch.nn as nn
from torch.autograd import Variable

from vanilla_rnn import VanillaRNN
from lstm import LSTM

################################################################################

def plot_grads(config):

	# Set random seed
	torch.manual_seed(config.random_seed)
	np.random.seed(config.random_seed)

	# Get input size
	if config.input_dim > 1:
		assert (config.input_dim == config.num_classes), "Cannot interpret input dimension %d"%(config.input_dim)
		input_dim = config.num_classes
	else:
		input_dim = config.input_dim

	# Init models
	rnn = VanillaRNN(config.input_length, input_dim, config.num_hidden, config.num_classes, requires_grad=True)
	lstm = LSTM(config.input_length, input_dim, config.num_hidden, config.num_classes, requires_grad=True)

	# Init data-set
	dataset = PalindromeDataset(config.input_length+1)

	data_loader_train = DataLoader(dataset, 1, num_workers=1)
	data_loader_test = DataLoader(dataset, 1, num_workers=1)

	# Setup the loss and optimizer
	criterion = torch.nn.CrossEntropyLoss()
	optimizer_rnn = torch.optim.RMSprop(rnn.parameters(), config.learning_rate)
	optimizer_lstm = torch.optim.RMSprop(lstm.parameters(), config.learning_rate)

	# Init avg gradients
	grads_magn_rnn = torch.zeros(config.input_length+1)
	grads_magn_lstm = torch.zeros(config.input_length+1)

	grads_rnn = torch.zeros(config.input_length+1, config.num_hidden)
	grads_lstm = torch.zeros(config.input_length+1, config.num_hidden)

	# Train the network a bit before plotting gradients (if needed)
	for step, (batch_inputs, batch_targets) in enumerate(data_loader_train):

		if step == config.train_steps:
			# If you receive a PyTorch data-loader error, check this bug report:
			# https://github.com/pytorch/pytorch/pull/9655
			break

		# Get model output (with one hot encoded input if requested)
		if config.input_dim > 1:
			x = torch.nn.functional.one_hot(batch_inputs.to(torch.int64), num_classes = config.num_classes).float()
		else:
			x = batch_inputs[:,:,None]

		# Train RNN
		p_rnn = rnn(x.to(config.device))[:,-1,:]

		loss_rnn = criterion(p_rnn, batch_targets)

		optimizer_rnn.zero_grad()
		loss_rnn.backward()

		torch.nn.utils.clip_grad_norm(rnn.parameters(), max_norm=config.max_norm)

		optimizer_rnn.step()

		# Train LSTM
		p_lstm = lstm(x.to(config.device))[:,-1,:]

		loss_lstm = criterion(p_lstm, batch_targets)

		optimizer_lstm.zero_grad()
		loss_lstm.backward()

		torch.nn.utils.clip_grad_norm(lstm.parameters(), max_norm=config.max_norm)

		optimizer_lstm.step()

		idx = int(step/config.train_steps*100)
		print("\rPretarining [step: %d/%d]\t loss rnn: %.3f - loss lstm: %.3f\t[%s]"%(step, config.train_steps, loss_rnn.detach().numpy(), loss_lstm.detach().numpy(), '='*(idx-1) + '>' + ' '*(100-idx-1)), end="")

	# Set example
	for step, (palindrome, target) in enumerate(data_loader_test):

		print("[%d/%d] Testing on palindrome:\nx:"%(step, config.test_steps), palindrome.int()[0].detach().numpy(), "\tt:", target.int()[0].detach().numpy())

		# Get model output (with one hot encoded input if requested)
		if config.input_dim > 1:
			x = torch.nn.functional.one_hot(palindrome.to(torch.int64), num_classes=config.input_dim).float()
		else:
			x = palindrome[:,:,None]

		# Run VanillaRNN on data-point input
		p_rnn = rnn(x)[:,-1,:]

		loss_rnn = criterion(p_rnn, target)

		rnn.zero_grad()
		loss_rnn.backward()

		# Run LSTM on data-point input
		p_lstm = lstm(x)[:,-1,:]

		loss_lstm = criterion(p_lstm, target)

		lstm.zero_grad()
		loss_lstm.backward()

		# Store gradients
		for t, tensor in enumerate(rnn.h_t):
			grads_magn_rnn[t] += torch.norm(tensor.grad).float()
			grads_rnn[t] += tensor.grad[0]

		for t, tensor in enumerate(lstm.h_t):
			grads_magn_lstm[t] += torch.norm(tensor.grad).float()
			grads_lstm[t] += tensor.grad[0]

		if step == config.test_steps:
		  break

	# Average gradients per time-step
	grads_magn_rnn /= config.test_steps
	grads_magn_lstm /= config.test_steps

	grads_rnn /= config.test_steps
	grads_lstm /= config.test_steps

	grads_magn_rnn = list(grads_magn_rnn.detach().numpy())
	grads_magn_lstm = list(grads_magn_lstm.detach().numpy())

	# Print results
	print("\nAverage gradients for RNN (per time-step:)")
	for t, gradient in enumerate(grads_magn_rnn):
		print("t(%d) : %.3f\t"%(t, gradient), end="")

	print("\nAverage gradients for LSTM (per time-step:)")
	for t, gradient in enumerate(grads_magn_lstm):
		print("t(%d) : %.3f\t"%(t, gradient), end="")
	print("")

	# Plot grads magnitudes

	fig = plt.figure()

	ax = fig.subplots(1)

	ax.plot(list(range(config.input_length)), grads_magn_rnn[1:], label="RNN")
	ax.plot(list(range(config.input_length )), grads_magn_lstm[1:], label="LSTM")

	ax.set_xlabel("time-step")
	ax.set_ylabel("h-gradient (magnitude)")

	ax.legend()

	ax.set_title(r"Gradients magnitudes (w.r.t. recurrent layer) of RNN vs LSTM"
			   "\n"
			  r"input_length=%d, num_tests=%d, input_dim=%d, random_seed=%d"%(config.input_length, config.test_steps, config.input_dim, config.random_seed))

	# Plot grad values
	fig = plt.figure()

	ax_rnn, ax_lstm = fig.subplots(1, 2)

	for t, grad in enumerate(list(grads_rnn)):
		ax_rnn.plot(list(range(config.num_hidden)), grad, label=("t=" + str(t)))

	for t, grad in enumerate(list(grads_lstm)):
		ax_lstm.plot(list(range(config.num_hidden)), grad, label=("t=" + str(t)))

	ax_rnn.set_xlabel("dimension")
	ax_rnn.set_ylabel("h-gradient")
	ax_rnn.legend()
	ax_rnn.set_title("RNN")

	ax_lstm.set_xlabel("dimension")
	ax_lstm.set_ylabel("h-gradient")
	ax_lstm.legend()
	ax_lstm.set_title("LSTM")

	plt.show()

if __name__ == "__main__":

	# Parse training configuration
	parser = argparse.ArgumentParser()

	# Model params
	parser.add_argument('--input_length', type=int, default=10, help='Length of an input sequence')
	parser.add_argument('--input_dim', type=int, default=1, help='Dimensionality of input sequence')
	parser.add_argument('--num_classes', type=int, default=10, help='Dimensionality of output sequence')
	parser.add_argument('--num_hidden', type=int, default=128, help='Number of hidden units in the model')
	parser.add_argument('--random_seed', type=int, default=0, help="Random seed used for torch")
	parser.add_argument('--max_norm', type=float, default=10.0)
	parser.add_argument('--train_steps', type=int, default=0, help='Number of training steps')
	parser.add_argument('--test_steps', type=int, default=100, help='Number of testing steps')
	parser.add_argument('--learning_rate', type=float, default=0.001, help='Learning rate')
	parser.add_argument('--device', type=str, default="cpu", help="Training device 'cpu' or 'cuda:0'")

	config = parser.parse_args()

	# Train the model
	plot_grads(config)
